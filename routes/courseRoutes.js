const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");



// router.post("/addCourse", (req, res) =>{
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })


// router.post("/addCourse", auth.verify, (req, res) => {

// 	const courseData = auth.decode(req.headers.authorization);
// 	// console.log(req.headers.authorization);
// 	// console.log(courseData.id);
// 	courseController.checkUserisAdmin({id: courseData.id}).then(resultFromController => res.send(resultFromController))
// })

router.post("/addCourse", auth.verify, (req, res) => {

	const courseData = auth.decode(req.headers.authorization);

	console.log(courseData.isAdmin);
	if (courseData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}

	
})


// Route for retrieving all courses
router.get("/allCourse", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
})





// Route for retrieving all active courses
router.get("/",  (req, res) => {



	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});






//Route for retrieving specific course
router.get("/:courseId", (req, res) =>{
	console.log(req.params.id)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
}) 



// Router for updating a course
router.put("/:courseId", auth.verify, (req, res) =>{

	let userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin) {
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}
	

})


module.exports = router;